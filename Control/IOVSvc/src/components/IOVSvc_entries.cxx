#include "../IOVSvc.h"
#include "../CondSvc.h"
#include "../IOVSvcTool.h"
#include "../CondInputLoader.h"
#include "../MetaInputLoader.h"

DECLARE_COMPONENT( IOVSvc )
DECLARE_COMPONENT( IOVSvcTool )

DECLARE_COMPONENT( CondSvc )

DECLARE_COMPONENT( CondInputLoader )
DECLARE_COMPONENT( MetaInputLoader )

